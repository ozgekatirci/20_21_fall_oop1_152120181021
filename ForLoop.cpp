#include <iostream>
#include <cstdio>
using namespace std;

int main() {
    int a, b, i;
    cin >> a >> b;
    //checking whether the given number is in range with if condition.
    if (a <= 0 || b <= 0) {
        cout << "please enter positive numbers." << endl;
    }
    //comparing given numbers
    if (a > b) {
        cout << "First number must be lower than second number." << endl;
    }
    else {
        for (i = a; i <= b; i++) {
            //printing result with using if condition
            if (1 <= i || i <= 9) {
                if (i == 1)
                    cout << "one" << endl;
                if (i == 2)
                    cout << "two" << endl;
                if (i == 3)
                    cout << "three" << endl;
                if (i == 4)
                    cout << "four" << endl;
                if (i == 5)
                    cout << "five" << endl;
                if (i == 6)
                    cout << "six" << endl;
                if (i == 7)
                    cout << "seven" << endl;
                if (i == 8)
                    cout << "eight" << endl;
                if (i == 9)
                    cout << "nine" << endl;
            }
            if (i > 9) {
                //determined whether the number is odd or even
                if (i % 2 == 0) {
                    cout << "even" << endl;
                }
                else {
                    cout << "odd" << endl;
                }
            }

        }
    }

    return 0;
}