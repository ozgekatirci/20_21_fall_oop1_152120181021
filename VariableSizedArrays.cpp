#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n, q, i, k, a, b;

    cin >> n;//the number of arrays.
	if (n < 1 || n>100000) {
		cout << "n is not  between 1 to 100000" << endl;
	}
    cin >> q;//control number
	if (n < 1 || n>100000) {
		cout << "q is not  between 1 to 100000" << endl;
	}
    vector<vector<int>>A(n);//2D vector 

    for (i = 0; i < n; i++) {
        cin >> k;//size of array
		if (n < 1 || n>300000) {
			cout << "k is not  between 1 to 300000" << endl;
		}
        A[i].resize(k);//with resize() we can arrange size of array.
        for (int j = 0; j < k; j++) {
            cin >> A[i][j];//
			if (A[i][j] < 0 || A[i][j]>1000000) {
				cout << "number is not  between 0 to 1000000" << endl;
			}
        }
    }
    for (i = 0; i < q; i++) {
        cin >> a;
        cin >> b;
        cout << A[a][b] << endl;
    }

    return 0;
}
