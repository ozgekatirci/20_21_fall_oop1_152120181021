#include <iostream>
#include <string>
using namespace std;

int main() {
    string a, b, c;
    char temp;
    int size1, size2;
    cin >> a;
    cin >> b;
    size1 = a.length();//length used to find the size of the string
    size2 = b.length();
    cout << size1 << " " << size2 << endl;
    c = a + b;
    cout << c << endl;
    temp = a[0];//The first elements of strings are replaced
    a[0] = b[0];
    b[0] = temp;
    cout << a << " " << b << endl;


    return 0;
}