#include <iostream>
#include <cstdio>
using namespace std;
//comparing numbers with funciton and finding maximum of numbers.
int max_of_four(int a, int b, int c, int d) {
    int max;
    //assuming method .
    max = a;
    //comparing numbers
    if (b > max) {
        max = b;
    }
    if (c > max) {
        max = c;
    }
    if (d > max) {
        max = d;
    }
    return max;
}


int main() {
    int a, b, c, d;
    scanf_s("%d %d %d %d", &a, &b, &c, &d);
    int ans = max_of_four(a, b, c, d);
    printf("%d", ans);

    return 0;
}