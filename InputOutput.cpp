#include <cmath>
#include <cstdio>
#include <iostream>
using namespace std;

int main() {
    int i, n, sum = 0;

    // the numbers can be printed and summed one by one with the for loop.
    for (i = 0; i < 3; i++) {
        cin >> n;
        sum += n;
    }

    cout << sum;

    return 0;
}
