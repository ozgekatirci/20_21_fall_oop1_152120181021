#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#define SIZE 1000
using namespace std;


int main() {
    int a, i, x;
    int A[SIZE];
    cin >> a;
    if (a<1 || a>SIZE) {
        cout << "please enter number between 1 to 1000" << endl;
    }
    else {

        for (i = 0; i < a; i++) {

            cin >> x;
            if (x >= 1 && x <= 10000) {
                //Using array we store numbers in a specific order
                A[i] = x;
            }
            else {
                //controlling whether the number is in range or not.
                cout << "the number isn't between 1 and 10000" << endl;
                i--;
            }

        }
        // printing numbers in reverse
        for (i = 0; i < a; i++) {
            cout << A[a - i - 1] << "\t";
        }
    }

    return 0;
}
