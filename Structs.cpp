#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

struct Student {
    //Structure members 

    //structure variables
    int age, standard;
    string first_name, last_name;
};
int main() {

    Student st;//defining a struct

    //can be access elements of struct with using struct.element.
    cin >> st.age >> st.first_name >> st.last_name >> st.standard;
    cout << st.age << " " << st.first_name << " " << st.last_name << " " << st.standard;

    return 0;
}