#include <iostream>
#include <cstdio>
using namespace std;

int main() {
    int a;
    long long  int b;
    char c;
    float d;
    double e;
    //numbers can get from user with cin
    cin >> a;
    cin >> b;
    cin >> c;
    cin >> d;
    cin >> e;

    
    printf("%d", a);
    printf("\n");

    printf("%lld", b);
    printf("\n");

    printf("%c", c);
    printf("\n");

    printf("%.3f", d);
    printf("\n");

    //With printf, we can specify which variable type to print and how many numbers to print after commas
    printf("%.9lf", e);
    printf("\n");

    return 0;
}
