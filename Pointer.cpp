#include <stdio.h>
#include<math.h>

void update(int* a, int* b) {
    //thanks to pointer updated memory adresses dynamically
    *a = *a + *b;
    *b = abs((*a - *b) - *b);

}

int main() {
    int a, b;
    //pointers are used to store and manage the addresses of dynamically allocated blocks of memory
    int* pa = &a, * pb = &b;

    scanf_s("%d %d", &a, &b);
    update(pa, pb);
    printf("%d\n%d", a, b);

    return 0;
}