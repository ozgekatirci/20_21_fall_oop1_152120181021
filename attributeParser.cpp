#include <iostream>
#include <map>
#include<string>
using namespace std;
//a map allows fast access to the value using the key and map ensures that a key is unique.
map <string, string> Key_Map;


//function to created map for each tag and assign tag name and value to map.
void Map_Tag(int& n, string tag) {
    int i = 1, k;
    string str, NameOfTag, attrbt, key;
    string temp = "";
    while (n > 0) {
        //take tag line
        getline(cin, str);

        // when closing tag is found tag name is updated.
        if (str[i] == '/') {
            while (str[i] != '>') i++;
            if (tag.size() > (i - 2))
                NameOfTag = tag.substr(0, tag.size() - i + 1);//parts that are not needed are removed from the string
            else
                NameOfTag = temp;
        }
        //when opening tag is found tag name is updated.
        else {
            while (str[i] != ' ' && str[i] != '>') i++;
            NameOfTag = str.substr(1, i - 1);  // parts that are not needed are removed from the string
            if (tag != temp) NameOfTag = tag + "." + NameOfTag;

            // get attribute values
            while (str[i] != '>') {
                k = ++i;
                while (str[i] != ' ') i++;
                //assign attribute name to map
                attrbt = str.substr(k, i - k);

                while (str[i] != '\"') i++;
                k = ++i;
                while (str[i] != '\"') i++;
                //  assign attribute value to map
                key = str.substr(k, i - k);
                i += 1;
                //assign name of tag and the value of tag.
                Key_Map[NameOfTag + "~" + attrbt] = key;
            }
        }
        n--;
        //The map is created as much as the given number and the same process continues for each map.
        Map_Tag(n, NameOfTag);//
    }
}

int main() {
    int n, query;
    cin >> n >> query;
    cin.ignore();
    //Maps as many as given number were created.
    string temp = "";
    Map_Tag(n, temp);

    string attrbt, key;
    while (query > 0) {
        //take queries
        getline(cin, attrbt);
        //The value corresponding to the attribute is found from the map
        key = Key_Map[attrbt];
        //if there isn't answer to query.
        if (key == temp) {
            key = "Not Found!";
        }
        cout << key << endl;
        query--;
    }
    return 0;
}
