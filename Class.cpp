#include <iostream>
#include <sstream>
using namespace std;
class Student {
 //class members

private://private members
    int val;
    stringstream str;
    string b;
public://public members
    int age, standard;
    string first_name, last_name;

    //public functions
    void set_age(int a) {//assigning numbers to struct members
        age = a;
    }
    int get_age() {//getting struct members
        return age;
    }
    void set_standard(int a) {
        standard = a;
    }
   
    int get_standard() {
        return standard;
    }
    void set_first_name(string a) {
        first_name = a;
    }
    string get_first_name() {
        return first_name;
    }
    void set_last_name(string a) {
        last_name = a;
    }
    string get_last_name() {
        return last_name;
    }
    string to_string() {//function to print strings with comma.
        string str, s;
        char c = ',';
        stringstream ss(str);
        ss << age << c << first_name << c << last_name << c << standard;
        ss >> s;

        return s;
    }

};

int main() {
    int age, standard;
    string first_name, last_name;

    cin >> age >> first_name >> last_name >> standard;

    Student st;//defining a structure

    //funtions to assign variables to structure members
    st.set_age(age);
    st.set_standard(standard);
    st.set_first_name(first_name);
    st.set_last_name(last_name);

    //functions to print struct members
    cout << st.get_age() << "\n";
    cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
    cout << st.get_standard() << "\n";
    cout << "\n";

    //function to print string with comma
    cout << st.to_string();

    return 0;
}
