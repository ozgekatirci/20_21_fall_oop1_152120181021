#include<iostream>
#include<fstream>
#include<stdlib.h>

using namespace std;

int Sum();
int Product();
float Average();
int Smallest();

int* A;
int DataSize;

int main()
{
	int x, count = 0, count1 = 0;
	fstream file("input.txt", ios::in);

	if (!file)
	{
		cout << "FILE ERROR!";
		exit(0);
	}

	file >> DataSize;
	A = new int[DataSize];

	for (int i = 0; i < DataSize; i++)
	{
		if (!file.eof()) {
			file >> A[i];
			count1++;
		}
	}

	while (!file.eof()) {
		file >> x;
		count++;
	}

	if (count1 != DataSize) {
		cout << "ERROR!You have entered number less than the number you specified." << endl;
	}
	else if (DataSize == 0) {
		cout << "ERROR!Please enter size of numbers." << endl;
	}

	else if (count > 0) {
		cout << "ERROR!You have entered number more than the size you specified." << endl;
	}

	else {

		cout << "Sum is " << Sum() << endl;
		cout << "Product is " << Product() << endl;
		cout << "Average is " << Average() << endl;
		cout << "Smallest is " << Smallest() << endl;
	}


	file.close();

	delete[] A;

	cout << endl << endl;
	system("pause");
}
//function to calculate sum of numbers
int Sum() {

	int sum = 0;

	for (int i = 0; i < DataSize; i++)
	{
		sum += A[i];
	}

	return sum;
}

//function to calculate production of numbers
int Product() {

	int i;
	int product = 1;

	for (i = 0; i < DataSize; i++)
	{
		product *= A[i];
	}

	return product;
}
//function to calculate average
float Average() {

	float average;

	average = (float)Sum() / DataSize;

	return average;
}
//function to calculate smallest
int Smallest() {

	int small;

	small = A[0];
	for (int i = 0; i < DataSize; i++)
	{
		if (A[i] < small) {
			small = A[i];
		}
	}

	return small;
}
