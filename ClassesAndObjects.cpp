#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cassert>
using namespace std;

class Student {
    int scores[5], i,a;
public:
    void input() {
        for (int i = 0; i < 5; i++) {
           
            cin >> a;
            // limit controlling 
            if (0<=a && a<=50) {
                scores[i] = a;
            }
            else {
                cout << "score must be between 0 and 50" << endl;
                i--;
            }
        }
    }
    int calculateTotalScore() {
        //sum of scores
        int total = 0;
        for (int i = 0; i < 5; i++) {
            total += scores[i];
        }
        return total;
    }

};

int main() {
    int n; // number of students
    cin >> n;
    if (n < 1 || n>100) {
        cout << "the number of student must be between 1 and 100" << endl;
    }
    Student* s = new Student[n]; // an array of n students

    for (int i = 0; i < n; i++) {
        s[i].input();
    }

    // assign kristen's score to struct member
    int kristen_score = s[0].calculateTotalScore();

    // determine how many students scored higher than kristen
    int count = 0;
    for (int i = 1; i < n; i++) {
        int total = s[i].calculateTotalScore();
        if (total > kristen_score) {
            count++;
        }
    }

    // print the number of students scored higher than kristen
    cout << count;

    return 0;
}
