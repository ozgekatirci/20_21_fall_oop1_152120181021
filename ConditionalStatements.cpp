#include <cstdio>
#include<iostream>

using namespace std;


int main()
{
    int n;
    cin >> n;
    //with if-else condition , we can arrange the process according to the conditions.
    if (1 <= n || n <= 9) {
        if (n == 1)
            cout << "one";
        if (n == 2)
            cout << "two";
        if (n == 3)
            cout << "three";
        if (n == 4)
            cout << "four";
        if (n == 5)
            cout << "five";
        if (n == 6)
            cout << "six";
        if (n == 7)
            cout << "seven";
        if (n == 8)
            cout << "eight";
        if (n == 9)
            cout << "nine";

    }
    if (n > 9) {
        cout << "Greater than 9";
    }
    if (n < 1 || n>100000000) {
        cout << "Please enter numbers between 1 to 10^9.";

    }

   

    return 0;
}
