#include<cstdio>
#include<iostream>

using namespace std;
class Box {
	//class members
private://private members
	int l, b, h;
public://public members

	//constructors

	//default constructor
	Box() {
		l = 0;
		b = 0;
		h = 0;
	}
	//parameterized constructor
	Box(int length, int breadth, int height) {
		l = length;
		b = breadth;
		h = height;
	}
	//copy constructor
	Box(const Box& B) {
		l = B.l;
		b = B.b;
		h = B.h;
	}
	//functions to get values
	int getLength() { return l; }
	int getBreadth() { return b; }
	int getHeight() { return h; }
	//function to calculate volume
	long long CalculateVolume() { return(long long)l * b * h; }

	//comparing Box A and B.
	friend bool operator<(Box& A, Box& B) {
		if ((A.l < B.l) || ((A.b < B.b) && (A.l == B.l)) || ((A.h < B.h) && (A.l == B.l) && (A.b == B.b))) {
			return true;
		}
		else {
			return false;
		}
	}

	//printing result
 friend ostream& operator<<(ostream & result, Box & B) {
			result << B.l << " " << B.b << " " << B.h;
			return result;
		};

};

void check2()
{
	int n;
	cin >> n;
	Box temp;//defining a class
	for (int i = 0; i < n; i++)
	{
		int type;
		cin >> type;
		if (type == 1)
		{
			cout << temp << endl;
		}
		if (type == 2)
		{
			int l, b, h;
			cin >> l >> b >> h;
			Box NewBox(l, b, h);
			temp = NewBox;
			cout << temp << endl;
		}
		if (type == 3)
		{
			int l, b, h;
			cin >> l >> b >> h;
			Box NewBox(l, b, h);
			if (NewBox < temp)
			{
				cout << "Lesser\n";
			}
			else
			{
				cout << "Greater\n";
			}
		}
		if (type == 4)
		{
			cout << temp.CalculateVolume() << endl;
		}
		if (type == 5)
		{
			Box NewBox(temp);
			cout << NewBox << endl;
		}

	}
}

int main()
{
	check2();
}