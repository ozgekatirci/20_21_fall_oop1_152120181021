#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {
    vector <int> vec;
    stringstream s(str);//Stringstream class is used for insertion and extraction of data to/from the string objects.s(str) Sets the contents of underlying string device object.
    int a;
    char ch;
    while (s >> a) {
        vec.push_back(a);//adding the element to the end of the string
        if (s.peek() == ',') {// peek() brings the next char.
            s.ignore();//ignoring (,).
        }
    }
    return vec;

}
int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for (int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }

    return 0;
}
